
Userspace Apache+PHP instance setup script
==========================================


Usage:
------

```
create docroot_path
```
where *docroot_path* is www root directory (where your *index.php* lives).
start and shutdown scripts will be copied to *docroot_path*/run/bin/

If *docroot_path* ends with a dash and four digits, it is taken as a port
to bind to. Otherwise you need to edit *docroot_path*/run/etc/apache2.conf
and set a port to bind to.

*apache2* and *php-fpm* are expected to be installed.
